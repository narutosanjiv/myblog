class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :content
      t.string :email
      t.string :name
      t.references :post
      t.references :blog

      t.timestamps
    end
    add_index :comments, :post_id
    add_index :comments, :blog_id
  end
end
