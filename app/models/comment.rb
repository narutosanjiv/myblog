class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :blog
  attr_accessible :content, :email, :name, :blog_id, :post_id
  validates_presence_of :content, :email, :name, :blog_id, :post_id
  validates :email, :format => {:with => Devise.email_regexp}
end
