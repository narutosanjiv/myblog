class User < ActiveRecord::Base
  
  has_one :blog
  
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :user_name, :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body
  
  validates_presence_of :user_name

  def subdomain
    self.blog.try(:subdomain)
  end

  def edit_post?(post)
    self.blog.posts.include?(post)
  end
end
