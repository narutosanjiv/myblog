class Post < ActiveRecord::Base
  belongs_to :blog
  has_many :comments
  
  attr_accessible :content, :title, :tag_list, :blog_id

  validates_presence_of :content, :title

  acts_as_taggable

  def comment?
    return !self.comments.blank?
  end
end
