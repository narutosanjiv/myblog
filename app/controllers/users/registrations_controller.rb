class Users::RegistrationsController < Devise::RegistrationsController

  # GET /resource/sign_up
  def new
    resource = build_resource({})
    respond_with resource
  end

end
