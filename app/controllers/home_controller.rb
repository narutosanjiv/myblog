class HomeController < ApplicationController
  include HomeHelper

  def index
    if current_user
      redirect_to user_blog_path(:username => current_user.subdomain)  
    end
  end

  def about

  end

  def contact 

  end
end
