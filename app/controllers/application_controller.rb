class ApplicationController < ActionController::Base
  include ApplicationHelper
  protect_from_forgery

  def after_sign_in_path_for(resource)

  
    if current_user.subdomain.blank?    
      current_user.build_blog({subdomain: 'narutosanjiv'}).save
    end
    user_blog_path(username: current_user.subdomain)
  end

  
end
