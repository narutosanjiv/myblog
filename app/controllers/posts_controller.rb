class PostsController < ApplicationController
  
  before_filter :authenticate_user!, :only => [:new, :create, :update, :destroy]
  respond_to :html, :json
  def new
    @post = Post.new 
  end

  def create
    puts "Parameter passed in #{params }" 
    @post =Post.new(params[:post])
    respond_to do|format|
      if @post.save
        format.html{redirect_to root_url and return } 
      else
        format.html{render 'posts/new'}
      end
    end
  end

  def edit 

  end

  def update
    @post = Post.find(params[:id])
    respond_to do|format|
      if @post.update_attributes(params[:post])
        format.html{redirect_to(@post, :notice => 'Post updated successfully')}
        format.json { respond_with_bip(@post) }
      else
        format.html {render 'post/new' }
        format.json{ respond_with @post }

      end
    end
  end
  
  def destroy 
    @post = Post.find(params[:id])
    @post_id = @post.id
    respond_to do|format|
      if @post.destroy
        format.js{render 'posts/destroy'}
      end
    end
  end

  def comment
    @post = Post.find(params[:id])
    @new_comment = Comment.new
    @comments = @post.comments.order('created_at desc')
    if request.post?
      
      respond_to do|format|
        @comment_to_save = Comment.new(params[:comment])
        @comment_to_save.post_id = @post.id
        @comment_to_save.blog_id = @post.blog.id
        if @comment_to_save.save
          @post = @post.reload
          @comments = @post.comments.order('created_at desc')
          format.js
        else
          format.js{ render 'error' }   
        end
      end
    end

  end

  def load_more_comment

  end
end
